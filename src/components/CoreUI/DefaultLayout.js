import React from 'react';
import PropTypes from 'prop-types';
import LeftSideBar from './LeftSideBar';
import TopHeader from './TopHeader';
import Breadcrumb from './Breadcrumb';
import AsideMenu from './AsideMenu';

class DefaultLayout extends React.Component {
  static propTypes = {
    children: PropTypes.element,
  };

  static defaultProps = {
    children: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      openMenu: true,
      openAside: false,
      sidebarMinimize: false,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.toggleAside = this.toggleAside.bind(this);
    this.toggleMinimize = this.toggleMinimize.bind(this);
  }

  toggleMenu() {
    if (this.state.openMenu) {
      this.setState({
        openMenu: false,
      });
    } else {
      this.setState({
        openMenu: true,
      });
    }
  }

  toggleAside() {
    if (this.state.openAside) {
      this.setState({
        openAside: false,
      });
    } else {
      this.setState({
        openAside: true,
      });
    }
  }

  toggleMinimize() {
    if (this.state.sidebarMinimize) {
      this.setState({
        sidebarMinimize: false,
      });
    } else {
      this.setState({
        sidebarMinimize: true,
      });
    }
  }

  render() {
    const openMenu = this.state.openMenu ? 'sidebar-lg-show' : '';
    const openAside = this.state.openAside ? 'aside-menu-lg-show' : '';
    const sidebarMinimize = this.state.sidebarMinimize ? 'brand-minimized sidebar-minimized' : '';
    return (
      <div className={`app header-fixed sidebar-fixed aside-menu-fixed ${openMenu} ${openAside} ${sidebarMinimize}`}>
        <TopHeader toggleMenu={this.toggleMenu} toggleAside={this.toggleAside} />
        <div className="app-body">
          <LeftSideBar open={this.state.openMenu} toggleMinimize={this.toggleMinimize} isMinimize={this.state.sidebarMinimize} />
          <main className="main">
            <Breadcrumb />
            <div className="container-fluid">
              {React.cloneElement(this.props.children, { ...this.props })}
            </div>
          </main>
          <AsideMenu open={this.state.openAside} />
        </div>
        <footer className="app-footer">
          <div>
            <a href="https://coreui.io"> CoreUI </a>
            <span> &copy; 2018</span>
          </div>
          <div className="ml-auto">
            <span>Powered by</span>
            <a href="https://coreui.io"> CoreUI </a>
          </div>
        </footer>
      </div>
    );
  }
}

export default DefaultLayout;
