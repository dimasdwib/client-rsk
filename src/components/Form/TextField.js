import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import cx from 'classnames';
import s from './TextField.css';

class TextField extends React.Component {
  constructor(props) {
    super(props);
    this.validationClass = this.validationClass.bind(this);
    this.exClass = this.exClass.bind(this);
    this.inputGroup = this.inputGroup.bind(this);
  }

  validationClass() {
    let { errorText, warningText, successText } = this.props;

    if (errorText && errorText !== '') {
      return 'has-error';
    } else if (warningText && warningText !== '') {
      return 'has-warning';
    } else if (successText && successText !== '') {
      return 'has-success';
    }
    return null;
  }

  exClass() {
    let exClass = ['form-control', s.TextField];

    if (this.props.className) {
      exClass.push(this.props.className);
    }

    exClass = exClass.join('');
    return exClass;
  }

  inputGroup(input) {
    if (this.props.inputGroupAddOnLeft || this.props.inputGroupAddOnRight) {
      let left = this.props.inputGroupAddOnLeft  ? <span className="input-group-addon"> {this.props.inputGroupAddOnLeft} </span> : '';
      let right= this.props.inputGroupAddOnRight ? <span className="input-group-addon"> {this.props.inputGroupAddOnRight} </span> : '';
      return (
        <div className="input-group">
          {left}
          {input}
          {right}
        </div>
      )
    }
    return input;
  }

  render() {
    const { inputGroupAddOnLeft, isRequired, inputGroupAddOnRight, LabelText, labelText, label, floatingLabelText, hintText, errorText, warningText, successText, ...properties } = this.props;
    const a = isRequired ? ' *' : null;
    return (
      <div className={ this.validationClass() }>
        { labelText ? (<label className="control-label">{ labelText } {a}</label>) : ''}
        { LabelText ? (<label className="control-label">{ LabelText } {a}</label>) : ''}
        { label ? (<label className="control-label">{ label } {a}</label>) : ''}
          { this.inputGroup(<input {...properties} autoComplete= "off" placeholder={hintText ? hintText : this.props.placeholder} className={this.exClass()} />) }
        { errorText && errorText!=='' ? (<span className="help-block"> {errorText} </span>) : '' }
        { warningText && warningText!=='' ? (<span className="help-block"> {warningText} </span>) : '' }
        { successText && successText!=='' ? (<span className="help-block"> {successText} </span>) : '' }
      </div>
    );
  }

}

export default withStyles(s)(TextField);
