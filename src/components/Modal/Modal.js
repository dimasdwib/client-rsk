import React from 'react';
import Rodal from 'rodal';

class Modal extends React.PureComponent {

  render() {
    return (
      <Rodal
        {...this.props}
        closeMaskOnClick={false}
        animation="slideUp"
        visible={this.props.open || this.props.visible}
      >
        { this.props.children }
      </Rodal>
    );
  }
}

export default Modal;