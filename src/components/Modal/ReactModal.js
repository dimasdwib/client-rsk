import React from 'react';
import Modal from 'react-responsive-modal';

class ReactModal extends ReactComponent {
  render() {
    return (
      <Modal
        {...this.props}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default ReactModal;