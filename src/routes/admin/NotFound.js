import React from 'react';

class NotFound extends React.Component {
  render() {
    return (
      <div className="animated fadeIn">
        <p> Not Found </p>
      </div>
    );
  }
}

export default NotFound;