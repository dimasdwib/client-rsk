import React from 'react';
import axios from 'axios';
import history from '../../../../history';
import TextField from '../../../../components/Form/TextField';
import LoadingButton from '../../../../components/Button/LoadingButton';
import Link from '../../../../components/Link';
import ReactSelect from '../../../../components/Form/ReactSelect';

class PermissionForm extends React.Component {
  static defaultProps = {
    params: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      group: '',
      roles: [],
      users: [],
      isLoadingSave: '',
      roleData: [],
      userData: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fetchPermission = this.fetchPermission.bind(this);
    this.fetchRole = this.fetchRole.bind(this);
    this.handleRole = this.handleRole.bind(this);
    this.handleUser = this.handleUser.bind(this);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.fetchPermission(this.props.params.id);
    }
    this.fetchRole();
    this.fetchUser();
  }

  /**
   * Fetch single permission
   */
  fetchPermission(id) {
    axios.get(`/permission/${id}`)
    .then((res) => {
      const users = [];
      const roles = [];
      res.data.users.map(u => {
        users.push({ value: u.id, label: u.name });
      });
      res.data.roles.map(r => {
        roles.push({ value: r.id, label: r.name });
      });
      this.setState({
        ...res.data,
        roles,
        users,
      });
    })
    .catch((err) => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchRole() {
    axios.get('role/all')
    .then(res => {
      this.setState({
        roleData: res.data,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchUser() {
    axios.get('users/all')
    .then(res => {
      this.setState({
        userData: res.data,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleRole(e) {
    this.setState({
      roles: e,
    });
  }

  handleUser(e) {
    this.setState({
      users: e,
    });
  }

  handleSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    const { name, group, roles, users } = this.state;
    const roleArr = [];
    const userArr = [];
    roles.forEach(r => {
      roleArr.push(r.label);
    });
    users.forEach(u => {
      userArr.push(u.value);
    });
    const data = {
      name,
      group,
      roles: roleArr.join(','),
      users: userArr.join(','),
    };

    // console.log(data);
    //
    // return;

    let req = axios.post('/permission', data);
    if (this.props.params.id) {
      data.id = this.props.params.id;
      req = axios.put(`/permission/${this.props.params.id}`, data);
    }

    this.setState({ isLoadingSave: true });
    req.then(res => {
      this.props.notify('success', res.data.message);
      this.setState({ isLoadingSave: false });
      history.push({
        pathname: `/admin/permissions/edit/${res.data.permission.id}`,
      });
    })
    .catch(err => {
      this.props.notify('error', err.response.data.message);
      this.setState({ isLoadingSave: false });
    });
  }

  render() {
    const { group, name, roleData, userData, users, roles } = this.state;
    let title = 'Create Permission';
    let buttonLabel = 'Save';
    if (this.props.params.id) {
      title = 'Edit Permission';
      buttonLabel = 'Update';
    }

    const roleOpt = [];
    const userOpt = [];
    roleData.forEach(r => {
      roleOpt.push({
        value: r.id,
        label: r.name,
      });
    });
    userData.forEach(u => {
      userOpt.push({
        value: u.id,
        label: u.name,
      });
    });

    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-12">
                    <h4 className="card-title mb-0">{ title }</h4>
                    <br />
                  </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <TextField
                        labelText="Name"
                        name="name"
                        onChange={this.handleChange}
                        value={name}
                        placeholder="Name"
                      />
                    </div>
                    {/*
                    <div className="form-group">
                      <TextField
                        labelText="Group"
                        name="group"
                        onChange={this.handleChange}
                        value={group}
                        placeholder="Permission Group"
                      />
                    </div>
                    */}
                    <div>
                      <LoadingButton
                        className="btn btn-primary"
                        onClick={this.handleSubmit}
                        isLoading={this.state.isLoadingSave}
                      >
                        {buttonLabel}
                      </LoadingButton>
                      &nbsp;
                      &nbsp;
                      <Link to="/admin/permissions">
                        <button disabled={this.state.isLoadingSave} className="btn btn-default">
                          Back
                        </button>
                      </Link>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <ReactSelect
                        labelText="Roles"
                        isMulti
                        value={roles}
                        options={roleOpt}
                        onChange={this.handleRole}
                      />
                    </div>
                    <div className="form-group">
                      <ReactSelect
                        labelText="Users"
                        isMulti
                        options={userOpt}
                        value={users}
                        onChange={this.handleUser}
                      />
                    </div>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PermissionForm;
