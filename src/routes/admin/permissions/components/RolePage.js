import React from 'react';
import axios from 'axios';
import { SyncLoader } from 'react-spinners';
import Link from '../../../../components/Link';
import TabMenu from './TabMenu';

class RolePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchRole: 'DONE',
      roleData: {},
    };
    this.fetchRole = this.fetchRole.bind(this);
    this.deleteRole = this.deleteRole.bind(this);
  }

  componentDidMount() {
    this.fetchRole();
  }

  deleteRole(role) {
    this.props.confirm(`Are you sure want to delete ${role.name} ?`)
    .then(() => {
      axios.delete(`/role/${role.id}`)
      .then(res => {
        this.fetchRole();
        this.props.notify('success', res.data.message);
      })
      .catch(err => {
        this.props.notify('error', err.response.data.message);
      });
    });
  }

  fetchRole() {
    this.setState({fetchRole: 'LOADING'});
    axios.get('/role')
    .then(res => {
      this.setState({
        roleData: res.data,
        fetchRole: 'DONE',
      });
    })
    .catch(err => {
      this.setState({fetchRole: 'ERROR'});
    });
  }

  render() {
    const { roleData, fetchRole } = this.state;
    const rowData = [];
    if (fetchRole === 'LOADING') {
      rowData.push(
        <tr key="loading">
          <td className="text-center" colSpan="6">
            <SyncLoader size={10} color="#d0d0d0" />
          </td>
        </tr>
      );
    }
    if (roleData.data) {
      roleData.data.forEach((role, i) => {
        rowData.push(
          <tr key={i}>
            <td> {i + 1} </td>
            <td> {role.name} </td>
            <td> - </td>
            <td>
              <Link to={`/admin/permissions/roles/edit/${role.id}`}><button className="btn btn-primary btn-sm"> <i className="fa fa-edit fa-fw" /> </button></Link>
              &nbsp;
              <button onClick={() => this.deleteRole(role)} className="btn btn-danger btn-sm"> <i className="fa fa-trash fa-fw" /> </button>
            </td>
          </tr>
        );
      });
    }
    return (
      <div className="animated fadeIn">
        <TabMenu active="roles" />
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="form-group">
                  <Link to="/admin/permissions/roles/new">
                    <button className="btn btn-primary"> Create New </button>
                  </Link>
                </div>
                <div className="table-responsive">
                  <table className="table table-compact">
                    <thead>
                      <tr>
                        <th> No </th>
                        <th> Name </th>
                        <th> User </th>
                        <th> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      {rowData}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RolePage;
