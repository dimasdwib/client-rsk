import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import history from '../../../../history';
import TextField from '../../../../components/Form/TextField';
import ReactSelect from '../../../../components/Form/ReactSelect';
import Link from '../../../../components/Link';
import LoadingButton from '../../../../components/Button/LoadingButton';

class UserPageEdit extends React.Component {
  static propTypes = {
    params: PropTypes.object,
    notify: PropTypes.func,
  };

  static defaultProps = {
    params: {},
    notify: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      username: '',
      email: '',
      password: '',
      passwordConfirm: '',
      isLoadingSave: false,
      changePassword: false,
      roleData: [],
      permissionData: [],
      role: [],
      permission: [],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchUser = this.fetchUser.bind(this);
    this.fetchRole = this.fetchRole.bind(this);
    this.fetchPermission = this.fetchPermission.bind(this);
    this.handleRole = this.handleRole.bind(this);
    this.handlePermission = this.handlePermission.bind(this);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.fetchUser(this.props.params.id);
    }
    this.fetchRole();
    this.fetchPermission();
  }

  /**
   * Fetch single user
   */
  fetchUser(id) {
    axios.get(`/users/${id}`)
    .then((res) => {
      const permission = [];
      res.data.permission.forEach((p) => {
        permission.push({
          label: p.name,
          value: p.id,
        });
      });
      const role = [];
      res.data.role.forEach((r) => {
        role.push({
          label: r,
          value: r,
        });
      });
      this.setState({
        ...res.data,
        role,
        permission,
      });
    })
    .catch((err) => {
      this.props.notify('error', err.response.data.message);
    });
  }

  fetchRole() {
    axios.get('/role/all')
    .then(res => {
      this.setState({ roleData: res.data });
    });
  }

  fetchPermission() {
    axios.get('/permission/all')
    .then(res => {
      this.setState({ permissionData: res.data });
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleRole(e) {
    this.setState({role: e});
  }

  handlePermission(e) {
    this.setState({permission: e});
  }

  handleSubmit(e) {
    if (e.preventDefault) {
      e.preventDefault();
    }

    const { name, username, email, password, role, permission } = this.state;
    const roleArr = [];
    const permissionArr = [];
    role.forEach(r => {
      roleArr.push(r.label);
    });
    permission.forEach(p => {
      permissionArr.push(p.label);
    });
    const data = {
      name,
      username,
      email,
      password,
      role: roleArr.join(','),
      permission: permissionArr.join(','),
    };

    let req = axios.post('/users', data);
    if (this.props.params.id) {
      data.id = this.props.params.id;
      req = axios.put(`/users/${data.id}`, data);
    }

    this.setState({ isLoadingSave: true });
    req.then(res => {
      this.props.notify('success', res.data.message);
      this.setState({ isLoadingSave: false });
      history.push({
        pathname: `/admin/users/edit/${res.data.user.id}`,
      });
    })
    .catch(err => {
      this.setState({ isLoadingSave: false });
      this.props.notify('error', err.response.data.message);
    });
  }

  render() {
    let title = 'Create User';
    let buttonLabel = 'Save';
    if (this.props.params.id) {
      title = 'Edit User';
      buttonLabel = 'Update';
    }

    const {
      email,
      username,
      name,
      password,
      passwordConfirm,
      changePassword,
      roleData,
      permissionData,
      role,
      permission,
    } = this.state;

    const roleOpt = [];
    const permissionOpt = [];
    roleData.forEach(r => {
      roleOpt.push({
        value: r.id,
        label: r.name,
      });
    });
    permissionData.forEach(p => {
      permissionOpt.push({
        value: p.id,
        label: p.name,
      });
    });

    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-12">
                    <h4 className="card-title mb-0">{title}</h4>
                    <br />
                  </div>
                </div>
                <form onSubmit={this.handleSubmit}>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <TextField
                          labelText="Full Name"
                          name="name"
                          onChange={this.handleChange}
                          value={name}
                          placeholder="Full Name"
                        />
                      </div>
                      <div className="form-group">
                        <div className="row">
                          <div className="col-md-6">
                            <TextField
                              labelText="Username"
                              name="username"
                              onChange={this.handleChange}
                              value={username}
                              placeholder="Username"
                            />
                          </div>
                          <div className="col-md-6">
                            <TextField
                              labelText="Email"
                              name="email"
                              onChange={this.handleChange}
                              value={email}
                              placeholder="Email"
                            />
                          </div>
                        </div>
                      </div>

                      {this.props.params.id ?
                        <div className="form-group">
                          <label>
                            <input
                              type="checkbox"
                              value={changePassword}
                              onChange={e => {
                                this.setState({ changePassword: e.target.checked });
                              }}
                            /> Change Password
                          </label>
                        </div>
                      : null}

                      {changePassword || !this.props.params.id ?
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-6">
                              <TextField
                                labelText="Password"
                                name="password"
                                onChange={this.handleChange}
                                value={password}
                                placeholder="Password"
                                type="password"
                              />
                            </div>
                            <div className="col-md-6">
                              <TextField
                                labelText="Confirm Password"
                                name="passwordConfirm"
                                onChange={this.handleChange}
                                value={passwordConfirm}
                                placeholder="Confirm Password"
                                type="password"
                              />
                            </div>
                          </div>
                        </div>
                      : null}

                      <div>
                        <LoadingButton
                          className="btn btn-primary"
                          onClick={this.handleSubmit}
                          isLoading={this.state.isLoadingSave}
                        >
                          {buttonLabel}
                        </LoadingButton>
                        &nbsp;
                        &nbsp;
                        <Link to="/admin/users">
                          <button disabled={this.state.isLoadingSave} className="btn btn-default">
                            Back
                          </button>
                        </Link>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <ReactSelect
                          labelText="Role"
                          options={roleOpt}
                          value={role}
                          onChange={this.handleRole}
                          isMulti
                        />
                      </div>
                      <div className="form-group">
                        <ReactSelect
                          labelText="Permission"
                          options={permissionOpt}
                          value={permission}
                          onChange={this.handlePermission}
                          isMulti
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserPageEdit;
