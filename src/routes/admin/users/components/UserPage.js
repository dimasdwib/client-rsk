import React from 'react';
import axios from 'axios';
import { SyncLoader } from 'react-spinners';
import Link from '../../../../components/Link';

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      fetchUser: 'LOADING',
    };

    this.deleteUser = this.deleteUser.bind(this);
  }

  componentDidMount() {
    this.fetchUser();
  }

  fetchUser() {
    this.setState({ fetchUser: 'LOADING' });
    axios.get('/users')
      .then(res => {
        this.setState({ fetchUser: 'DONE' });
        this.setState({
          userData: res.data,
        });
      })
      .catch(err => {
        this.setState({ fetchUser: 'ERROR' });
        console.log(err);
      });
  }

  deleteUser(user) {
    this.props.confirm(`Are you sure want to delete ${user.name} ?`)
    .then(() => {
      axios.delete(`/users/${user.id}`, { id: user.id })
      .then(res => {
        this.props.notify('success', res.data.message);
        this.fetchUser();
      })
      .catch(err => {
        this.props.notify('error', err.response.data.message);
      });
    });
  }

  render() {
    const { userData, fetchUser } = this.state;
    const rowData = [];
    if (fetchUser === 'LOADING') {
      rowData.push(
        <tr key="loading">
          <td className="text-center" colSpan="6">
            <SyncLoader size={10} color="#d0d0d0" />
          </td>
        </tr>
      );
    }
    if (userData.data) {
      userData.data.forEach((user, i) => {
        rowData.push(
          <tr key={i}>
            <td> {i + 1} </td>
            <td> {user.name} </td>
            <td> {user.username} </td>
            <td> {user.roles.map(role => (<li>{role.name}</li>))}</td>
            <td> {user.permissions.map(permission => (<li>{permission.name}</li>))} </td>
            <td>
              <Link to={`/admin/users/edit/${user.id}`}><button className="btn btn-primary btn-sm"> <i className="fa fa-edit fa-fw" /> </button></Link>
              &nbsp;
              <button onClick={() => this.deleteUser(user)} className="btn btn-danger btn-sm"> <i className="fa fa-trash fa-fw" /> </button>
            </td>
          </tr>
        );
      });
    }
    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="form-group">
                  <Link to="/admin/users/new">
                    <button className="btn btn-primary"> Create New </button>
                  </Link>
                </div>
                <div className="table-responsive">
                  <table className="table table-compact">
                    <thead>
                      <tr>
                        <th> No </th>
                        <th> Name </th>
                        <th> Username </th>
                        <th> Role </th>
                        <th> Direct Permission </th>
                        <th> Action </th>
                      </tr>
                    </thead>
                    <tbody>
                      {rowData}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserPage;
